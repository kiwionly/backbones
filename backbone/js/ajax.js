
$(document).ready(function(){
  
	var app = {};
	
	app.property = Backbone.Model.extend({
			
		
	});
	
	
	app.properties = Backbone.Collection.extend({		
		model : app.property,
		url: '/api/feed',
		parse: function(response)
		{
			console.log(response);
			
			return response;
		}
	});
	
	
	app.view = Backbone.View.extend({
				
		el: "#wrapper",	
		
		initialize : function() {
//			this.collection = new app.properties();
			
			this.source   = $("#template").html();
			this.template = Handlebars.compile(this.source);
		},
		
		events :{
			"click #call" : "call"	
		},
		
		render : function()
		{						
			var context = list.models[0].toJSON();
			var html    = this.template(context);
			
			$("#container").html(html);
						
			return this;
		}, 
		
		call : function()
		{
			this.collection.fetch({
				data : $.param({page : 1})
			}).always(function(data) {
				app.view.render()
			});
		}
		
	});
	
	var list = new app.properties();
	
	app.view = new app.view({
		model : app.property,			
		collection: list
	});
	
	Backbone.history.start();
});