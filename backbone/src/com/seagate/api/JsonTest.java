package com.seagate.api;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;

public class JsonTest {

	public static void main(String[] args) {
		List<Property> list = new ArrayList<Property>();
		
		Property p = new Property();
		p.setName("gurney drive");
		p.setUrl("image1");
		
		list.add(p);
		
		p = new Property();
		p.setName("gurney plaza");
		p.setUrl("image2");
		
		list.add(p);
		
		String text = JSON.toJSONString(list);		
		System.out.println(text);
	}
}
