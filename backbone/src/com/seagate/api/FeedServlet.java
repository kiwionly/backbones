package com.seagate.api;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

@WebServlet(urlPatterns="/feed")
@SuppressWarnings("serial")
public class FeedServlet extends HttpServlet
{
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		
		
		Writer out = response.getWriter();
		
		List<Property> list = new ArrayList<Property>();
		
		Property p = new Property();
		p.setName("gurney drive");
		p.setUrl("image1");
		
		list.add(p);
		
		p = new Property();
		p.setName("gurney plaza");
		p.setUrl("image2");
		
		list.add(p);
		
		String text = JSON.toJSONString(list);		

		out.append(text);
		
		response.setContentType("application/json");
		out.close();
	}

}
